*Problem 1 - Assignment 1

Sets
*The 25 players are implemented
i index of player /1*25/
*The positions are implemented
j position /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
*The formations are implemented
k formation /442,352,4312,433,343,4321/

*The quality players are implemented
qua(i) quality players /13,20,21,22/
*The strength players are implemented
str(i) strength players /10,12,23/
;

Table
a(k,j) Matrix for formation and position showing how many players (i) there has to be employed at position (j) at a given formation (k)
         GK      CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
442      1       2       1       1       2       1       1       0       2       0
352      1       3       0       0       3       1       1       0       1       1
4312     1       2       1       1       3       0       0       1       2       0
433      1       2       1       1       3       0       0       0       1       2
343      1       3       0       0       2       1       1       0       1       2
4321     1       2       1       1       3       0       0       2       1       0
;
Table
b(i,j) Matrix over fitness player-role showing how "good" each player is at every position
         GK     CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
1        10      0       0       0       0       0       0       0       0       0
2        9       0       0       0       0       0       0       0       0       0
3        8.5     0       0       0       0       0       0       0       0       0
4        0       8       6       5       4       2       2       0       0       0
5        0       9       7       3       2       0       2       0       0       0
6        0       8       7       7       3       2       2       0       0       0
7        0       6       8       8       0       6       6       0       0       0
8        0       4       5       9       0       6       6       0       0       0
9        0       5       9       4       0       7       2       0       0       0
10       0       4       2       2       9       2       2       0       0       0
11       0       3       1       1       8       1       1       4       0       0
12       0       3       0       2       10      1       1       0       0       0
13       0       0       0       0       7       0       0       10      6       0
14       0       0       0       0       4       8       6       5       0       0
15       0       0       0       0       4       6       9       6       0       0
16       0       0       0       0       0       7       3       0       0       0
17       0       0       0       0       3       0       9       0       0       0
18       0       0       0       0       0       0       0       6       9       6
19       0       0       0       0       0       0       0       5       8       7
20       0       0       0       0       0       0       0       4       4       10
21       0       0       0       0       0       0       0       3       9       9
22       0       0       0       0       0       0       0       0       8       8
23       0       3       1       1       8       4       3       5       0       0
24       0       3       2       4       7       6       5       6       4       0
25       0       4       2       2       6       7       5       2       2       0
;

Variables
*The objective variable is implemented
z total reward;


Binary variable
*The variable for a chosen player is implemented
x(i,j) is player i used on position j
*The variable for a chosen formation is implemented
y(k) is formation k used;

equations
*The objective function is implemented
reward           total reward
leastone(i)      atleast one player per position
formation        we can only have one formation and must have one
allpositions(j)  all position must be filled out
balance          to secure balance (for quality and strength players) as there are 4 quality players the slack variable "3" upholds the balance here
onequality       atleast one quality player;

*Reward is the objective function, which objective is to maximize the total fitness player-role
reward..            z =e= sum((i,j), x(i,j)*b(i,j));
*Leastone makes sure that atleast one player (i) is chosen to a position (j)
leastone(i)..       sum(j,x(i,j)) =l= 1;
*Formation makes sure that one and only one formation (k) is chosen
formation..         sum(k,y(k)) =e= 1;
*Allpositions makes sure that all positions (j) in the chosen formation (k) is filled out by a player chosen to the given position
allpositions(j)..   sum(k,y(k)*a(k,j)) =e= sum(i,x(i,j));
*Balance makes sure that, if all quality players are employed, at least one strength player is employed
*In this equation the "3" is added due to the occasion where all the quality players are employed but only one strength player is employed
balance..           sum((j,qua(i)),x(i,j)) =l= sum((j,str(i)),x(i,j))+3;
*Onequality makes sure that atleast one quality player is picked.
onequality..        sum((j,qua(i)),x(i,j)) =g= 1;



MODEL production /all/;
SOLVE production USING mip MAXIMIZING z;

DISPLAY
         x.l
         y.l
         z.l;
