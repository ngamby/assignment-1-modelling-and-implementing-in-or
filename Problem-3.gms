*Problem 3 - Assignment 1

Sets
*Implementing the 5 ships
v the five different ships /1*5/
*Implementing the 2 routes
r the 2 possible routes /Asia, ChinaPacific/
*Implementing the 8 ports
p the 8 different ports /Singapore, Incheon, Shanghai, Sydney, Gladstone,
Dalian, Osaka, Victoria/
*Implementing the first incompatible pair of ports
h1(p) first incompatible pair of ports /Singapore, Osaka/
*Implementing the second incompatible pair of ports
h2(p) second incompatible pair of ports /Incheon, Victoria/
;

Parameters
*Implementing the different parameters given in the assignment
F(v) The fixed cost in million dollars to use ship v
/
1 65
2 60
3 92
4 100
5 110
/

G(v) How many days in a year can ship v sail
/
1 300
2 250
3 350
4 330
5 300
/

D(p) if port p is visited it would have to be visited at least D(p) times
/
Singapore        15
Incheon          18
Shanghai         32
Sydney           32
Gladstone        45
Dalian           32
Osaka            15
Victoria         18
/

M an upper bound for how many routes a ship may take - not set in the assignment but can make the solution differ
/1000/
;
*By setting it to 1000, the program should just choose the optimal amount of routes for each ship

Table
*Implementing the different tables given in the assignment
c(v,r) the cost for ship v to complete one time route r in million dollars
         Asia    ChinaPacific
1        1.41    1.9
2        3       1.5
3        0.4     0.8
4        0.5     0.7
5        0.7     0.8
;

Table
t(v,r) the number of days needed to complete one time route r
         Asia    ChinaPacific
1        14.4    21.2
2        13      20.7
3        14.4    20.6
4        13      19.2
5        12      20.1
;

Table
a(p,r) matrix of routes and ports - is 1 if route r passes through port p and 0 otherwise
                 Asia    ChinaPacific
Singapore        1       0
Incheon          1       0
Shanghai         1       1
Sydney           0       1
Gladstone        0       1
Dalian           1       1
Osaka            1       0
Victoria         0       1
;

Variables
z total reward;

Integer variable
x(v,r) does ship v sails at route r
;

Binary variables
y(v)   is ship v used
q(p)   is port p used


equations
reward           total reward
ydef(v)          to define y
timebound(v)     applying the time bound a ship can't sail more than G(v) days a year
leastvisit(p)    applying that a chosen port has to be visited d(p) times
incompatible1    implementing the first incompatible set of ports
incompatible2    implementing the second incompatible set of ports
fiveports        implementing that atleast 5 ports has to be visited
;

*Reward is the objective function which objective is to minimize the total yearly cost
reward..         z=e= sum((v,r),c(v,r)*x(v,r))+sum(v,f(v)*y(v));
*Ydef is an equation which define the variable "y" and making sure that each ship (v) used cannot sail more than "M" routes
*unfortunately i was unable to find an equation to merge ydef and timebound.
ydef(v)..        y(v)*M =g= sum(r,x(v,r));
*Timebound makes sure that a chosen ship (v) cannot exceed the amount of days it can sail in a year
timebound(v)..   sum(r,t(v,r)*x(v,r)) =l= g(v);
*Leastvisit makes sure that a chosen port (p) is visited atleast d(p) times
leastvisit(p)..  sum((v,r),x(v,r)*a(p,r)) =g= d(p)*q(p);
*Incompatible1 and Incompatible2 define the incompability of the two pairs of incompatible ports
*by making sure if one of the ports is used, the other port will remain unused
incompatible1..  sum(h1(p),q(p)) =l= 1;
incompatible2..  sum(h2(p),q(p)) =l= 1;
*Fiveports makes sure that atleast 5 ports is visited
fiveports..      sum(p,q(p)) =g= 5;


MODEL production /all/;
SOLVE production USING mip MiniMIZING z;

DISPLAY
         x.l
         y.l
         q.l
         z.l;
