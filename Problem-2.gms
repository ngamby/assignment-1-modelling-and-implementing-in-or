*Problem 2 - Assignment 1

sets
*The 8 nodes is implemented
i index of nodes /1*8/
*The 30 keys are implemented
k index of key /1*30/
;
*Copying set i to make set j
alias(i,j);

Parameters
mk the amount of memory each key occupy /186/
Mi the amount of memory that each node has /700/
T the limit of how many times a key can be used /4/
q the amount of keys to share for a direct communication /3/
;

Variables
z total reward;

Binary variables
x(i,k) is key k contained in node i
y(i,j) is there a direct connection between node i and node j
u(i,j,k) is key k contained in node i AND in node j
;

equations
reward                   total reward
onlyTtimes(k)            each key can only be used T times
Mspace(i)                each node can only store M(i) data
udef1(i,j,k)             equation 1 to define u(ijk)
udef2(i,j,k)             equation 2 to define u(ijk)
*linkfornodes1(i,j)       equation 1 to define a direct connection
linkfornodes2(i,j)       equation 2 to define a direct connection
;

*Reward is the objective function which objective is to maximize the number of direct connenctions (y)
*As there is no reason to sum over the same set of nodes, some of the following equations only sum over the sets, where the order of node i is less than the order of node j
reward..                 z=e=sum((i,j)$(ord(i) lt ord(j)),y(i,j));
*OnlyTtimes makes sure that each key (k) can only be used T times
onlyTtimes(k)..          sum(i,x(i,k)) =l= T;
*Mspace makes sure that each node (i) does not exceed a memory-storage larger than Mi
Mspace(i)..              sum(k,x(i,k))*mk =l= Mi;
*Udef1 is a lowerbound in the definition of the variable "u"
udef1(i,j,k)..           (x(i,k)+x(j,k))$(ord(i) lt ord(j)) =l= 1 + u(i,j,k)$(ord(i) lt ord(j));
*Udef2 is an upperbound in the definition of the variable "u"
udef2(i,j,k)..           (x(i,k)+x(j,k))$(ord(i) lt ord(j)) =g= 2 * u(i,j,k)$(ord(i) lt ord(j));

*Linkfornodes1 was made as a lowerbound in the definition of when two nodes had a direct connection
*but as stated, we are solving a maximizing problem and accordingly the objective function will try to set "y" to one when it can
*linkfornodes1(i,j)..     sum(k, u(i,j,k))$(ord(i) lt ord(j)) - (q-1) =l= y(i,j)$(ord(i) lt ord(j));

*Linkfornodes2 is an upperbound in the definition of when two nodes have a direct connection
*it is stating that for two nodes (i & j) to have a direct connection, the nodes must have atleast "q" keys in common
linkfornodes2(i,j)..     sum(k, u(i,j,k))$(ord(i) lt ord(j)) =g= q*y(i,j)$(ord(i) lt ord(j));



MODEL production /all/;
SOLVE production USING mip MAXIMIZING z;
*
DISPLAY
         x.l
         y.l
         u.l
         z.l;
